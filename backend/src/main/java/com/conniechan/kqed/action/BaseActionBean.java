package com.conniechan.kqed.action;

import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;

public class BaseActionBean implements ActionBean {
  private ActionBeanContext context;

  public void setContext(ActionBeanContext context) {
    this.context = context;
  }

  public ActionBeanContext getContext() {
    return context;
  }

  @DefaultHandler
  public Resolution view() {
    return new ForwardResolution("/WEB-INF/jsp/" + getClass().getSimpleName() + ".jsp");
  }

  public String getJsName() {
    return "/frontend/" + getClass().getSimpleName() + ".js";
  }
}
