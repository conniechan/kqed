package com.conniechan.kqed.action;

import java.io.File;
import java.net.InetAddress;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import com.conniechan.kqed.pbsapi.CallsignsClient;
import com.conniechan.kqed.pbsapi.Feed;
import com.conniechan.kqed.pbsapi.TVSClient;
import com.google.gson.Gson;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.AddressComponent;
import com.google.maps.model.AddressComponentType;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.record.Postal;

import net.sourceforge.stripes.action.ErrorResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.StreamingResolution;
import net.sourceforge.stripes.validation.Validate;

public class HomeAction extends BaseActionBean {
  private static final Gson GSON = new Gson();

  @Validate(required = true, on = "getDayListings")
  private String callsign;
  @Validate(required = true, on = "getDayListings")
  private Date date;
  private double latitude;
  private double longitude;
  @Validate(required = true, on = "getCallsigns")
  private String postalCode;

  public Resolution getDayListings() {
    List<Feed> feeds = TVSClient.getInstance().getDayListings(callsign, date);
    feeds = feeds.stream().filter(feed -> {
      return (feed.getAnalogChannel() == null || feed.getAnalogChannel().isEmpty())
          && feed.getDigitalChannel() != null && !feed.getDigitalChannel().isEmpty();
    }).collect(Collectors.toList());
    return new StreamingResolution("application/json", GSON.toJson(feeds));
  }

  public Resolution getCallsigns() {
    List<String> callsigns = CallsignsClient.getInstance().getCallsigns(postalCode);
    return new StreamingResolution("application/json", GSON.toJson(callsigns));
  }

  public Resolution getGeoIpLocation() {
    Postal postalCode = null;
    try {
      File database = new File(getClass().getResource("/geoip.mmdb").toURI());
      DatabaseReader reader = new DatabaseReader.Builder(database).build();
      String ipAddress = getContext().getRequest().getRemoteAddr();
      CityResponse response = reader.city(InetAddress.getByName(ipAddress));
      postalCode = response.getPostal();
      return new StreamingResolution("application/json", GSON.toJson(postalCode
          .getCode()));
    } catch (Exception e) {
      System.out.println(e);
      return new ErrorResolution(HttpServletResponse.SC_NOT_FOUND);
    }
  }

  public Resolution getPostalCodeFromLatLng() {
    String postalCode = null;
    try {
      GeoApiContext context = new GeoApiContext().setApiKey(System.getenv(
          "GOOGLE_MAPS_KEY"));
      LatLng location = new LatLng(latitude, longitude);
      GeocodingResult[] results = GeocodingApi.reverseGeocode(context, location).await();
      Optional<AddressComponent> optionalZip = Arrays.stream(results).flatMap(
          result -> Arrays.stream(result.addressComponents)).filter(addressComponent -> {
            return addressComponent.types[0] == AddressComponentType.POSTAL_CODE;
          }).findFirst();
      if (optionalZip.isPresent()) {
        postalCode = optionalZip.get().shortName;
      }
    } catch (Exception e) {
      // TODO
      System.out.println(e);
    }
    return new StreamingResolution("application/json", GSON.toJson(postalCode));
  }

  public void setCallsign(String callsign) {
    this.callsign = callsign;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public void setLatitude(double latitude) {
    this.latitude = latitude;
  }

  public void setLongitude(double longitude) {
    this.longitude = longitude;
  }

  public void setPostalCode(String postalCode) {
    this.postalCode = postalCode;
  }
}
