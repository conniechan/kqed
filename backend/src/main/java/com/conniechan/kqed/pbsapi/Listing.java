package com.conniechan.kqed.pbsapi;

import java.time.LocalTime;

public class Listing {
  private LocalTime startTime;
  private int minutes;
  private String title;
  private String episodeTitle;
  private String episodeDescription;
  private String description;
}
