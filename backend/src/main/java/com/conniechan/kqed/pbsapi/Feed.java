package com.conniechan.kqed.pbsapi;

public class Feed {
  private String fullName;
  private String digitalChannel;
  private String analogChannel;
  private Listing[] listings;

  public String getDigitalChannel() {
    return digitalChannel;
  }
  
  public String getAnalogChannel() {
    return analogChannel;
  }
  
  public Listing[] getListings() {
    return listings;
  }
}
