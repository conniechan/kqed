package com.conniechan.kqed.pbsapi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.google.gson.FieldNamingStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class CallsignsClient {
  private static CallsignsClient instance;
  private static final FieldNamingStrategy FIELD_NAMING_STRATEGY = new FieldNamingStrategy() {
    @Override
    public String translateName(Field f) {
      if (!f.getName().equals("callsign")) {
        return "$" + f.getName();
      }
      return f.getName();
    }
  };
  
  private String baseUrl;
  
  public CallsignsClient() {
    baseUrl = "http://services.pbs.org/callsigns/";
  }

  public static CallsignsClient getInstance() {
    if (instance == null) {
      instance = new CallsignsClient();
    }
    return instance;
  }
  
  public List<String> getCallsigns(String zip) {
    HttpURLConnection connection = null;
    try {
      URL url = new URL(baseUrl + "zip/" + zip + ".json");
      connection = (HttpURLConnection) url.openConnection();
      BufferedReader responseReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
      Gson gson = new GsonBuilder()
          .setFieldNamingStrategy(FIELD_NAMING_STRATEGY)
          .create();
      ApiResponse response = gson.fromJson(responseReader, ApiResponse.class);
      return Arrays.stream(response.getItems())
          .flatMap(item -> {
            return Arrays.stream(item.getLinks());
            })
          .map(link -> {
            return link.getCallsign();
            })
          .collect(Collectors.toList());
    } catch (IOException e) {
      // TODO
    } finally {
      if (connection != null) {
        connection.disconnect();
      }
    }
    return null;
  }

  private class ApiResponse {
    private CallSign2ZipMapping[] items;
    
    public CallSign2ZipMapping[] getItems() {
      return items;
    }
  }
}
