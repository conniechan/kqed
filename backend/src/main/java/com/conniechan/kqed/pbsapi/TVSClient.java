package com.conniechan.kqed.pbsapi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

public class TVSClient {
  private static TVSClient instance;

  private String apiKey;
  private String baseUrl;

  private TVSClient() {
    apiKey = System.getenv("PBS_TVS_KEY");
    baseUrl = "http://services.pbs.org/tvss/";
  }
  
  public static TVSClient getInstance() {
    if (instance == null) {
      instance = new TVSClient();
    }
    return instance;
  }
  
  public List<Feed> getDayListings(String callsign, Date date) {
    HttpURLConnection connection = null;
    try {
      URL url = new URL(baseUrl + callsign + "/day/" + new SimpleDateFormat("yyyyMMdd").format(date));
      connection = (HttpURLConnection) url.openConnection();
      connection.setRequestProperty("X-PBSAUTH", apiKey);
      BufferedReader responseReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
      JsonDeserializer<LocalTime> timeDeserializer = new JsonDeserializer<LocalTime>() {
        @Override
        public LocalTime deserialize(JsonElement json, Type typeOfT,
            JsonDeserializationContext context) throws JsonParseException {
          String value = json.getAsString();
          int hour = Integer.parseInt(value.substring(0, 2));
          int minute = Integer.parseInt(value.substring(2));
          return LocalTime.of(hour, minute);
        }
      };
      Gson gson = new GsonBuilder()
          .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
          .registerTypeAdapter(LocalTime.class, timeDeserializer)
          .create();
      ApiResponse response = gson.fromJson(responseReader, ApiResponse.class);
      return Arrays.asList(response.getFeeds());
    } catch (IOException e) {
      System.out.println(e);
      // TODO
    } finally {
      if (connection != null) {
        connection.disconnect();
      }
    }
    return null;
  }

  private class ApiResponse {
    private Feed[] feeds;

    public Feed[] getFeeds() {
      return feeds;
    }
  }
}
