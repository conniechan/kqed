<%@ taglib prefix="stripes" uri="http://stripes.sourceforge.net/stripes.tld" %>

<stripes:layout-render name="/WEB-INF/jsp/base.jsp">
  <stripes:layout-component name="contents">
    <div id='react-root'></div>
  </stripes:layout-component>
</stripes:layout-render>
