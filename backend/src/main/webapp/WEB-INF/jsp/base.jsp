<%@ taglib prefix="stripes" uri="http://stripes.sourceforge.net/stripes.tld" %>

<stripes:layout-definition>
  <html>
    <head>
    </head>
    <body>
      <stripes:layout-component name="contents" />
      <script type='text/javascript' src='${actionBean.jsName}'></script>
    </body>
  </html>
</stripes:layout-definition>
