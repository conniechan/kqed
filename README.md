# TV/Radio Scheduler #

This is an app that shows public broadcasting schedules for the user's location. It is a full stack solution. It contains a calendar for the user to select a date (defaults to today), a zip code input, and shows all the listings for the chosen day in a table form.

On the front end, it uses React, babel, webpack, and LESS. The backend uses the Java 8 and Stripes framework to serve pages, with Maven as the build tool. I used Heroku for deployment. Third party resources include Infinite Calendar, PBS's TVSS API and Callsigns API, Google Maps Geocoding API, and MaxMind GeoIP.

A live version of this can be found at https://stormy-mountain-23465.herokuapp.com/Home.action

## Omissions ##
I did not pull in radio schedules I was unable to find an adequate API that provides radio schedules (NPR is still working on their NPR One API that would provide schedules and station lookups but is months away from such an API). Thus, this only includes PBS TV schedules.

## Getting Started ##
#### Prerequisites ####
Apache Maven, Heroku CLI
### Configs ###
Secrets are kept in environment variables instead of code/version history. The following must be set:
```
#!bash

export PBS_TVS_KEY="pbs_key"
export GOOGLE_MAPS_KEY="google_maps_key"
```
### Build ###
In the root directory, run `mvn install`. 
### Deploy ###
Run `heroku local web` then navigate to http://localhost:5000/Home.action

## Geolocation ##
This provides stations in a specific location rather than all stations in the country. It uses two methods to geolocate the users. First, it uses MaxMind's GeoIP to autodetect the user's location based on IP address. However, this is not always exact (MaxMind provides a big database for download, in which their consumers would lookup IP addresses). The autodetection is just to provide a best guess so that the UI is full on initial load and lessens the need for user interaction. If the guess is wrong or doesn't exist, then the user can click the map pointer icon to use the browser's geolocation feature to provide a more exact location. Then it converts the latitude and longitude to a zip code using Google Maps Geocoding API. The user can also type in their zip code.

Once we have a zip code, we determine what the PBS station callsigns are in the area with PBS's callsigns API. With the callsigns in hand, we can use the TVSS API to find schedules.

## Desired Changes ##
If I had more time, I would implement a cache layer between the front end and the API since loading the schedules is fairly time intensive. A cache would ensure speedier load times for multiple people who look up schedules in the same zip code for the same day.