import './Location.less';
import '../../font-awesome/css/font-awesome.min.css';

import React from 'react';
import reqwest from 'reqwest';

class Location extends React.Component {
  componentDidMount() {
    if (this.props.loadingLocationCallback) {
      this.props.loadingLocationCallback(true);
    }
    reqwest('?getGeoIpLocation')
      .then(function(postalCode) {
        this.props.setLocation(postalCode);
      }.bind(this)).catch(function() {
        this.props.loadingLocationCallback(false);
      }.bind(this));
  }

  getCurrentPosition() {
    return new Promise((resolve, reject) => {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(resolve, reject);
      } else {
        reject();
      }
    });
  }
  
  onChange(evt) {
    var enteredValue = evt.target.value;
    if (/^\d*$/.test(enteredValue)) {
      this.props.setLocation(enteredValue);
    }
  }

  pinpointLocation() {
    if (this.props.loadingLocationCallback) {
      this.props.loadingLocationCallback(true);
    }
    this.getCurrentPosition().then(currentPosition => {
      return reqwest('?getPostalCodeFromLatLng&latitude=' + currentPosition.coords.latitude + '&longitude=' + currentPosition.coords.longitude)
    }).then(function(postalCode) {
      this.props.setLocation(postalCode);
    }.bind(this)).catch(function() {
      this.props.this.props.loadingLocationCallback(false);
    }.bind(this));
  }
  
  render() {
    return (
      <div className='postalCodeContainer'>
        <input className='postalCode'
          onChange={this.onChange.bind(this)}
          placeholder='Zip Code'
          type='text'
          value={this.props.postalCode}
        />
        <div className='pinpoint fa-map-marker'
          onClick={this.pinpointLocation.bind(this)}
        />
      </div>
    );
  }
}
Location.propTypes = {
  loadingLocationCallback: React.PropTypes.func,
  postalCode: React.PropTypes.string,
  setLocation: React.PropTypes.func.isRequired
}
Location.defaultProps = {
  postalCode: ''
}

export default Location;
