import React from 'react';
import InfiniteCalendar from 'react-infinite-calendar';
import 'react-infinite-calendar/styles.css';
import './DateSelector.less';

class DateSelector extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showCalendar: false
    };
  }
  
  format(dateObject) {
    return (dateObject.getMonth() + 1) + '/' + dateObject.getDate() + '/' + dateObject.getFullYear();
  }
  
  onFocus() {
    this.setState({
      showCalendar: true
    });
  }

  onSelect(date) {
    this.props.setDate(date.toDate());
    this.setState({
      showCalendar: false
    });
  }

  render() {
    return (
      <div className='dateContainer'>
        <input
          className='dateInput'
          type='text'
          value={this.format(this.props.date)}
          onFocus={this.onFocus.bind(this)}
          readOnly={true}
        />
        {this.state.showCalendar &&
          <InfiniteCalendar
            afterSelect={this.onSelect.bind(this)}
            className='calendar'
            selectedDate={this.props.date}
            overscanMonthCount={2}
          />
        }
      </div>
    );
  }
}
DateSelector.propTypes = {
  date: React.PropTypes.instanceOf(Date).isRequired,
  setDate: React.PropTypes.func.isRequired
}

export default DateSelector
