import './Spinner.less';

import React from 'react';

class Spinner extends React.Component {
  render() {
    return (
      <div className='overlay'>
        <div className='container'>
          <div className='spinner' />
        </div>
      </div>
    );
  }
}

export default Spinner;
