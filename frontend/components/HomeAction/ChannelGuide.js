import './ChannelGuide.less';

import React from 'react';

class Time extends React.Component {
  renderHalfHour(hourInt) {
    var timeString = Math.floor(hourInt / 2) + ':';
    if (hourInt % 2 !== 0) {
      timeString += '30';
    } else {
      timeString += '00';
    }
    return <div className='hour'>{timeString}</div>;
  }

  render() {
    return (
      <div className='time'>
        <div className='headerColumn'></div>
        {Array(48).fill().map((value, i) => this.renderHalfHour(i) )}
      </div>
    );
  }
}

class Feed extends React.Component {
  render() {
    var listings = this.props.info.listings.map(listing => {
      return (
        <Listing
          info={listing}
        />
      );
    });
    return (
      <div className='feed'>
        <div className='feedIdentifier headerColumn'>
          <div className='channel'>{this.props.info.digitalChannel}</div>
          <div className='name'>{this.props.info.fullName}</div>
        </div>
        <div className='listings'>
          {listings}
        </div>
      </div>
    );
  }
}
Feed.propTypes = {
  info: React.PropTypes.shape({
    digitalChannel: React.PropTypes.string,
    fullName: React.PropTypes.string.isRequired,
    listings: React.PropTypes.array.isRequired
  }).isRequired
}

class Listing extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  closeDetails(evt) {
    this.setState({
      showDetails: false
    });
    evt.stopPropagation();
    evt.preventDefault();
  }

  getAirTime() {
    var startTime = this.props.info.startTime;
    var durationMinutes = this.props.info.minutes;
    var date = new Date();
    date.setHours(startTime.hour);
    date.setMinutes(startTime.minute);
    var airTime = date.getHours();
    if (date.getMinutes()) {
      airTime += ':';
      if (date.getMinutes() < 10) {
        airTime += '0';
      }
      airTime += date.getMinutes();
    }
    airTime += '-';
    date.setMinutes(date.getMinutes() + durationMinutes);
    airTime += date.getHours();
    if (date.getMinutes()) {
      airTime += ':';
      if (date.getMinutes() < 10) {
        airTime += '0';
      }
      airTime += date.getMinutes();
    }
    return airTime;
  }

  showDetails() {
    this.setState({
      showDetails: true
    });
  }

  render() {
    var width = (this.props.info.minutes / 30) * 100;
    var left = ((this.props.info.startTime.hour * 60 + this.props.info.startTime.minute) / 30) * 100;
    var style = {
      left: left + 'px',
      width: width + 'px'
    };
    var details = null;
    if (this.state.showDetails) {
      details = (
        <div className='detailsBubble'>
          <div className='close'
            onClick={this.closeDetails.bind(this)}
          >
            &#x2715;
          </div>
          <div className='title'>{this.props.info.title}</div>
          {this.props.info.episodeTitle && <div className='subtitle'>{this.props.info.episodeTitle}</div>}
          <div>{this.getAirTime()}</div>
          <div className='description'>{this.props.info.episodeDescription || this.props.info.description}</div>
        </div>
      );
    }
    return (
      <div className='listing'
        onClick={this.showDetails.bind(this)}
        style={style}
      >
        <div className='title' title={this.props.info.title}>{this.props.info.title}</div>
        <div className='subtitle'>{this.getAirTime()}</div>
        {details}
      </div>
    );
  }
}
Listing.propTypes = {
  info: React.PropTypes.shape({
    minutes: React.PropTypes.number,
    startTime: React.PropTypes.shape({
      hour: React.PropTypes.number.isRequired,
      minute: React.PropTypes.number.isRequired
    }),
    title: React.PropTypes.string
  }).isRequired
}

class ChannelGuide extends React.Component {
  render() {
    return (
      <div className='guide'>
        <Time />
        {this.props.feeds && this.props.feeds.map(feed => <Feed info={feed} />)}
      </div>
    );
  }
}

ChannelGuide.propTypes = {
  feeds: React.PropTypes.array
}

export default ChannelGuide;
