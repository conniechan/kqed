import './HomeAction.less';

import React from 'react';
import {render} from 'react-dom';
import reqwest from 'reqwest';
import DateSelector from '../components/HomeAction/DateSelector'
import Location from '../components/HomeAction/Location';
import ChannelGuide from '../components/HomeAction/ChannelGuide';
import Spinner from '../components/HomeAction/Spinner';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      date: new Date(),
      postalCode: ''
    };
  }
  
  componentWillUpdate(nextProps, nextState) {
    if (nextState.date != this.state.date || nextState.postalCode != this.state.postalCode) {
      if (nextState.postalCode.length === 5) {
        this.setState({
          loading: true
        });
        this.reqwestPromise({
          url: '?getCallsigns',
          data: {
            postalCode: nextState.postalCode
          }
        }).then(callsigns => {
          var listingRequests = callsigns.map(callsign => {
            return this.reqwestPromise({
              url: '?getDayListings',
              data: {
                callsign: callsign,
                date: (nextState.date.getMonth() + 1) + ' ' + nextState.date.getDate() + ' ' + nextState.date.getFullYear()
              }
            });
          });
          return Promise.all(listingRequests);
        }).then(feedArrays => {
          var allFeeds = [].concat.apply([], feedArrays);
          allFeeds = allFeeds.sort(function(f1, f2) {
            var f1Channel = parseFloat(f1.digitalChannel);
            var f2Channel = parseFloat(f2.digitalChannel);
            if (f1Channel < f2Channel) {
              return -1;
            } else if (f1Channel > f2Channel) {
              return 1;
            } else {
              return 0;
            }
          });
          this.setState({
            feeds: allFeeds,
            loading: false
          });
        });
      }
    }
  }

  loadingLocationCallback(loading) {
    this.setState({
      loading: loading
    });
  }

  setDate(date) {
    this.setState({
      date: date
    });
  }
  
  setPostalCode(postalCode) {
    if (/^\d*$/.test(postalCode)) {
      this.setState({
        postalCode: postalCode
      });
    }
  }

  reqwestPromise(payload) {
    return new Promise((resolve, reject) => {
      reqwest(payload).then(resolve, reject);
    });
  }

  render() {
    return (
      <div>
        {this.state.loading && <Spinner />}
        <div className='toolbar'>
          <DateSelector
            date={this.state.date}
            setDate={this.setDate.bind(this)}
          />
          <Location
            loadingLocationCallback={this.loadingLocationCallback.bind(this)}
            postalCode={this.state.postalCode}
            setLocation={this.setPostalCode.bind(this)}
          />
        </div>
        <ChannelGuide feeds={this.state.feeds} />
      </div>
    );
  }
}

render(<App />, document.getElementById('react-root'));
