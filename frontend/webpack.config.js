var glob = require('glob');

var jsPaths = glob.sync('./entry/**/*.js');
var entry = {};
jsPaths.forEach(function(path) {
  var fileName = /([^./]+)\.js$/.exec(path)[1];
  entry[fileName] = path;
});

module.exports = {
  entry: entry,
  output: {
    path: './bundle',
    filename: '[name].js'
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node/,
        loader: 'babel-loader'
      },
      {
        test: /\.less$/,
        exclude: /node/,
        loader: 'style!css!less'
      },
      {
        test: /\.css$/,
        loader: 'style!css'
      },
      {
        test: /\.(eot|woff2?|ttf|svg)($|\?)/,
        loader: 'file-loader'
      }
    ]
  }
}
